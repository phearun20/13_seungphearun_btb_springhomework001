package com.example.demorestapi;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
public class CustomerController {
    int id ;
    ArrayList<Customer> customers = new ArrayList<>();
    public CustomerController(){
        customers.add(new Customer(1,"Dararith", 25,"M", "PP"));
        customers.add(new Customer(2,"Visal", 30,"M", "SR"));
        customers.add(new Customer(3, "Songha", 26, "M", "KPS"));
        id=customers.size();

    }
    @GetMapping("/customers")
    public ResponseEntity<?> getAllProduct(){
        return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
                LocalDateTime.now(),
                200,
                "Successfully get product",
                customers
        ))  ;
    }

    // insert to arraylist
    @PostMapping("/customers")
    public ResponseEntity<?> insertCustomer(@RequestBody CustomerRequest customer){
        id++;
        customers.add(new Customer(id, customer.getName(),customer.getAge(),customer.getGender(),customer.getAddress()));
        return ResponseEntity.ok(new CustomerResponse(LocalDateTime.now(), 200,"Successfully insert Customer", customer));
    }

    // get product by id
    @GetMapping("/customers/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") Integer proId){
        for (Customer pro: customers){
            if(pro.getId() == proId){
                return ResponseEntity.ok(new CustomerResponse(LocalDateTime.now(), 200, "Successfully get id", pro));
            }
        }
        return (ResponseEntity<?>) ResponseEntity.notFound();
    }

    // find product by name
    @GetMapping("/customers/search")
    public ResponseEntity<?> findCustomerByName(@RequestParam String name){
        for (Customer pro: customers){
            if (pro.getName().equals(name)) {
                return ResponseEntity.ok(new CustomerResponse(LocalDateTime.now(), 200, "Successfully get Name", pro));
            }
        }
        return (ResponseEntity<?>) ResponseEntity.notFound();
    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<?> updateCustomer (@PathVariable Integer id, @RequestBody CustomerRequest customerRequest) {
            for(Customer customer: customers){
                if(customer.getId().equals(id)){
                    customer.setName(customerRequest.getName());
                    customer.setAge(customerRequest.getAge());
                    customer.setGender(customerRequest.getGender());
                    customer.setAddress(customerRequest.getAddress());
                return ResponseEntity.ok(new CustomerResponse( LocalDateTime.now(),200,"Update successfully",customer));
                }

            }

        return ResponseEntity.ok(ResponseEntity.notFound());
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity<?> deleteCustomer (@PathVariable Integer id){
        customers.remove(id-1) ;
        return ResponseEntity.ok(new CustomerResponse(LocalDateTime.now(), 200, "Delete Successfully", customers));

    }





}
